$(document).ready(function(){

  var helloContent = {
    0: "Hello, guys!",
    1: "Hello, bros!",
    2: "Hello, devs!",
    3: "Hello, friends!",
    4: "Hello, fans!",
    5: "Hello, stalkers!",
    6: "Hello, you!",
    7: "Hello, you!",
    8: "Hello, you!",
    9: "Hello, you!",
    10: "Hello, you!",
    11: "Hello, you!",
    12: "Hello, there!",
  }

  var descContent = {
    "en": "I'm Ahmad Banyu Rachman, a software developer from Indonesia. I have interest in all about multimedia visual: graphic design, typography, animation, cinematic video, etc. But, inadvertently I went into Computer Science majors and stuck in this coding world. My final project is about parallel computing using R language, but UI/UX design remains the thing I love the most. I like to watch football even though I can not play it, and I really like music and play some instruments as a hobby. I'm a big fan of Muse and Sherlock Holmes!",
    "id": "Saya Ahmad Banyu Rachman, seorang pengembang perangkat lunak dari Indonesia. Saya tertarik pada semua tentang multimedia visual: desain grafis, tipografi, animasi, video sinematik, dll. Namun, tidak sengaja saya masuk ke jurusan Ilmu Komputer dan terjebak dalam dunia pengkodean ini. Proyek akhir saya adalah tentang komputasi paralel menggunakan bahasa R, tetapi desain UI/UX tetap menjadi hal yang paling saya gemari. Saya suka menonton sepak bola meskipun saya tidak bisa bermain, dan saya sangat suka musik dan memainkan beberapa instrumen sebagai hobi. Saya penggemar berat Muse dan Sherlock Holmes!"
  }

  $('#mainContent').html(helloContent[11]);
  $('#mainContent').html("Ahoy!");
  $('#descContent').html(descContent.en);

  $('.id.flag').on('click', function(){
    $('#descContent').html(descContent.id);
    $('#descContent').transition('jiggle', '500ms');
  });

  $('.gb.flag').on('click', function(){
    $('#descContent').html(descContent.en);
    $('#descContent').transition('jiggle', '500ms');
  });

  $('#brImage').transition('set looping').transition('shake', '8000ms');

  var countContent = 0;

  // var oneSec = setInterval(function(){
  //   $('#mainContent').html(helloContent[countContent]);
  //   countContent += 1;
  //   if (countContent > 12) {
  //     countContent = 0;
  //   }
  // }, 500);

});
